## Vue - Django 연동과정
* vue.js 에서 메뉴를 클릭 하면 server 에 request 하고 django 는 HttpRespnse 클래스로
response(html/css/Vue.js) 응답한다.

* 응답받아 rendering 하며 vue.js 코스를 실행하여 axios.get(/api/xxxx/) 요청하며 
django 의 JsonResponse 서브클래스가 JosonResponse(200, 리스트) 로 응답한다.

* 추가 버튼을 클릭시 axios.post(/api/xxxx/) server 에 요청 django 의 jsonResponse 클래스가
JsonResponse(201, 추가신규) 로 응답한다.

* 삭제 버튼을 클릭시 axios.delete(/api/xxxx/) server 에 요청 django 의 jsonResponse 클래스가
JsonResponse(204) 로 응답한다.

