from django.contrib.admindocs.views import TemplateView
from django.shortcuts import render


class TodoTV(TemplateView):
    template_name = 'todo/todo_index.html'

