import json

from django.forms import model_to_dict
from django.http import JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import ensure_csrf_cookie
from django.views.generic.edit import BaseDeleteView, BaseCreateView
from django.views.generic.list import BaseListView

from todo.models import Todo


@method_decorator(ensure_csrf_cookie, name='dispatch')
class ApiTodoVL(BaseListView):
    model = Todo
    # template_name  JsonResponse 로 보내기 때문에 필요없음

    # https://docs.djangoproject.com/en/3.1/ref/request-response/#jsonresponse-objects
    # 아래 get method 는 임시로 만든 data 이기 때문에 table 에서 가져오도록 ListView 에서 가져오도록 수정해야 한다.
    # def get(self, request, *args, **kwargs):
    #     tmpList = [
    #         {'id': 1, 'name': '도무지f', 'todo': 'django 는 vue 와 연동하여 만들기'},
    #         {'id': 2, 'name': '주니만f', 'todo': '물량으로 사람을 움직이게 할까?'},
    #         {'id': 3, 'name': '가여운f', 'todo': '약은 얼마나 먹어야 하나?'},
    #     ]
    #     # JsonResponse 로 data 를 보낼때 딕션너리가 아니기 때문에 safe 를 False 로 한다.
    #     return JsonResponse(data=tmpList, safe=False)

    # 아래 사이트에서 클래스형 ListView 에서 JsonResponse 를 보내기 위해 수정할 곳을 찾는다.
    # http://ccbv.co.uk/projects/Django/3.0/django.views.generic.list/ListView/
    def render_to_response(self, context, **response_kwargs):
        # context 변수안에 object_list 로 가져오 data 를 value 로 딕셔너리로 해체하여 list type 로 만든다.
        todoList = list(context['object_list'].values())
        return JsonResponse(data=todoList, safe=False)


# @method_decorator(csrf_exempt, name='dispatch')
class ApiTodoDelV(BaseDeleteView):
    model = Todo

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        self.object.delete()
        return JsonResponse(data={}, status=204)


# @method_decorator(csrf_exempt, name='dispatch')
class ApiTodoCV(BaseCreateView):
    model = Todo
    fields = '__all__'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['data'] = json.loads(self.request.body)
        return kwargs

    def form_valid(self, form):
        print('form_valid(): ', form)
        self.object = form.save()
        newTodo = model_to_dict(self.object)
        print(f'newTodo: {newTodo}')
        return JsonResponse(data=newTodo, status=201)

    def form_invalid(self, form):
        print('form_invalid(): ', form)
        print('form_invalid(): ', self.request.POST)
        print('form_invalid(): ', self.request.body.decode('utf8'))
        return JsonResponse(data=form.errors, status=400)